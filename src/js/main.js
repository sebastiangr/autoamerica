$(document).ready(function() {

    var browserHeight = $(window).height();
    var viewportHeight = $(document).height();
    var sideNav = $('.side-nav');
    var navScrollHidden = (viewportHeight - browserHeight - 400);
    var searchBtn = $('.search-btn');
    var searchBar = $('#search');
    var dropdownBarBtn = $('.dropdown-btn');
    var dropdownBar = $('#dropdown-bar');

    $(window).scroll(function() {
        if ($(document).scrollTop() > navScrollHidden) {
            $(sideNav).addClass("side-nav-off");
        } else {
            $(sideNav).removeClass("side-nav-off");
        }
    });

    // $(searchBtn).click(function(){
    //     if($(this).hasClass("collapsed")){
    //         searchBar.slideDown(200);
    //         $(this).removeClass("collapsed");
    //     }
    //     else
    //     {
    //         searchBar.slideUp(200);
    //         $(this).addClass("collapsed");
    //     }
    //     return false;
    // });

    $(".tab-close a").click(function() {
        $(this).closest(".dropdown-menu").prev().dropdown("toggle");
    });

    // $(dropdownBarBtn).click(function(){
    //     if($(this).hasClass("collapsed")){
    //         dropdownBar.slideDown(400);
    //         $(this).removeClass("collapsed");
    //         console.log("Hi!");
    //     }
    //     else
    //     {
    //         dropdownBar.slideUp(400);
    //         $(this).addClass("collapsed");
    //         console.log("Bye!");
    //     }
    //     return false;
    // });


    size_li_brands = $(".search-list-brands li").size();
    x = 5;
    $('.search-list-brands li:lt('+x+')').show();
    $('#view-more-brands').click(function () {
        x= (x+5 <= size_li_brands) ? x+5 : size_li_brands;
        $('.search-list-brands li:lt('+x+')').show();
    });

    size_li_years = $(".search-list-years li").size();
    x = 5;
    $('.search-list-years li:lt('+x+')').show();
    $('#view-more-years').click(function () {
        x= (x+5 <= size_li_years) ? x+5 : size_li_years;
        $('.search-list-years li:lt('+x+')').show();
    });

})